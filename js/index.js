$('.carousel').carousel({
    interval: 2000
});

//JQuery con el modal
$('#contactoAgente').on('show.bs.modal',function (e){
    console.log('El modal se esta mostrando');

    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-warning');
    $('#contactoBtn').prop('disabled',true);
});
$('#contactoAgente').on('shown.bs.modal',function (e){
    console.log('El modal se mostró');
});
$('#contactoAgente').on('hide.bs.modal',function (e){
    console.log('El modal se esta ocultando');
});
$('#contactoAgente').on('hidden.bs.modal',function (e){
    console.log('El modal se oculto');

    $('#contactoBtn').removeClass('btn-warning');
    $('#contactoBtn').addClass('btn-outline-success');
    $('#contactoBtn').prop('disabled',false);
});